public class HelloWorldx10
{
    public static void main(String[] args)
    {
        // Prints "Hello, World" in the terminal window 10 times.
        // Without for loop
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
        System.out.println("Hello, World");
    }
}