public class x34ThreeSort
{
    public static void main(String args[])
    {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);
        int z = Integer.parseInt(args[2]);

        // One
        int tmp = Math.min(x,y);
        y = Math.max(x,y);
        x = tmp;

        // Two
        tmp = Math.min(y,z);
        z = Math.max(y,z);
        y = tmp;

        // Three
        tmp = Math.min(x,y);
        y = Math.max(x,y);
        x = tmp;

        System.out.println(x + ", " + y + ", " + z);
    }
}