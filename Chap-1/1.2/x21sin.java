public class x21sin
{
    public static void main(String args[])
    {
        double theta = Double.parseDouble(args[0]);
        double value = Math.sin(2*theta) + Math.sin(3*theta);
        System.out.println(value);
    }
}