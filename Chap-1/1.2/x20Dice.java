public class x20Dice
{
    public static void main(String[] args)
    {
        int min = 1;
        int max = 6;
        double r_1 = Math.random();
        double r_2 = Math.random();
        // uniform between 0.0 and 1.0
        int value = (int) ((r_1 + r_2) * (max- min + 1)) + (2 * min); // uniform between 0 and n-1
        System.out.println(value);
    }
}
