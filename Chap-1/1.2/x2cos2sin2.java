public class x2cos2sin2
{
    public static void main(String[] args)
    {
        double theta = Double.parseDouble(args[0]);
        double sin_2 = Math.sin(theta) * Math.sin(theta);
        double cos_2 = Math.cos(theta) * Math.cos(theta);
        double sum = sin_2 + cos_2;
        System.out.println(sum);
    }
}