public class x15ThreeInts
{
    public static void main(String args[])
    {
        int one = Integer.parseInt(args[0]); 
        int two = Integer.parseInt(args[1]);
        int three = Integer.parseInt(args[2]);
        boolean triangle = (one < (two + three)) && (two < (one + three)) && (three < (one + two));
        System.out.println(triangle);
    }
}