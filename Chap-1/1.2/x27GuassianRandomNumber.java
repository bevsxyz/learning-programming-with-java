public class x27GuassianRandomNumber
{
    public static void main(String args[])
    {
        double u = Math.random();
        double v = Math.random();
        // Box-muller formula
        double r = Math.sin(2* Math.PI * v) * Math.sqrt(-2 * Math.log(u));
        System.out.println(r);
    }
}