public class x14EvenDivision
{
    public static void main(String args[])
    {
        int one = Integer.parseInt(args[0]);
        int two = Integer.parseInt(args[1]);
        boolean even = (one % two == 0) || (two % one == 0);
        System.out.println(even); 
    }
}