public class x31mercatorProjection
{
    public static void main(String args[])
    {
        double lamda_0 = Double.parseDouble(args[0]);
        double phi = Double.parseDouble(args[1]); // Latitude
        double lamda = Double.parseDouble(args[2]); // Longitude
        double x = lamda - lamda_0;
        double y = (1.0/2.0) * Math.log((1.0 + Math.sin(phi) / (1.0 - Math.sin(phi))));
        System.out.println(x + ", " + y);
        // I am not  sure
    }
}