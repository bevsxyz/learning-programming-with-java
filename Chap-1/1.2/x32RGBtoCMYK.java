public class x32RGBtoCMYK
{
    public static void main(String args[])
    {
        // How to tackle rgb all 0 without conditionals?
        int r = Integer.parseInt(args[0]);
        int g = Integer.parseInt(args[1]);
        int b = Integer.parseInt(args[2]);
        double w = Math.max((r/255.0), (g/255.0));
        w = Math.max(w, (b/255.0));
        double c = (w - (r/255)) / w;
        double m = (w - (g/255)) / w;
        double y = (w - (g/255)) / w;
        double k = 1- w;
        System.out.println(c + ", " + m + ", " + y  + ", " + k);
    }
}