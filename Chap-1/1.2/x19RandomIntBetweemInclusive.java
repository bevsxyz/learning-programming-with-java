public class x19RandomIntBetweemInclusive
{
    public static void main(String[] args)
    {
        int min = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[1]);
        double r = Math.random();
        // uniform between 0.0 and 1.0
        int value = (int) (r * (max- min + 1)) + min; // uniform between 0 and n-1
        System.out.println(value);
    }
}