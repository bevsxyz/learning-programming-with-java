public class x24ContinuouslyCompoundedInterest
{
    public static void main(String args[])
    {
        double t = Double.parseDouble(args[0]);
        double P = Double.parseDouble(args[1]);
        double r = Double.parseDouble(args[2]);
        // P + interest = P * e^rt
        double money = P * Math.exp(r * t);
        System.out.println(money);
    }
}